package net.ponycraft.lobe.nametags.bukkit;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import net.milkbowl.vault.chat.Chat;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import net.ponycraft.lobe.nametags.bukkit.MetaChannelListener;

public final class PlayerNametags extends JavaPlugin implements Listener {
	public PlayerNametags instance = this;
	private static final Logger log = Logger.getLogger("Minecraft");

	private static final String channelData = "pcraftnt_data";
	private static final String channelMeta = "pcraftnt_meta";

	private Long delayTicks;

	public MetaChannelListener meta = new MetaChannelListener(this, channelMeta);
	public BukkitTask task = null;

	public Map<String, String> prefixes = new HashMap<>();
	public Map<String, String> displayNames = new HashMap<>();
	public Map<String, String> suffixes = new HashMap<>();

	public static Chat chat = null;

	@Override
	public void onEnable() {
		this.saveDefaultConfig();
		delayTicks = Long.valueOf(this.getConfig().getInt("delayTicks"));

		if (!setupChat()) {
			log.severe(String.format("[%s] - Could not initialize Vault due to missing dependency!", getDescription().getName()));
			getServer().getPluginManager().disablePlugin(this);
			return;
		}

		getServer().getPluginManager().registerEvents(this, this);
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, channelData);

		scheduleTask();
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("playernametags")) {
			if (args.length != 1) {
				sender.sendMessage("Invalid number of arguments!");
				return false;
			}
			if (args[0].equalsIgnoreCase("reload")) {
				if (sender instanceof Player) {
					if (!sender.hasPermission("PlayerNametags.reload")) {
						sender.sendMessage("You do not have permission to access this command");
						return true;
					}
				}
				reloadPlugin();
				sender.sendMessage("Configuration reloaded.");
				return true;
			}
			if (args[0].equalsIgnoreCase("info")) {
				if (sender instanceof Player) {
					if (!sender.hasPermission("PlayerNametags.info")) {
						sender.sendMessage("You do not have permission to access this command");
						return true;
					}
				}
				sender.sendMessage(String.format("Task ID is %d", task.getTaskId()));
				sender.sendMessage(String.format("Delay is %d ticks", delayTicks));
				return true;
			}
		}
		return false;
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onQuit(PlayerQuitEvent event) {
		meta.unregisterPlayer(event.getPlayer());
	}

	public class ScheduledTask extends BukkitRunnable {

		@Override
		public void run() {
			// First clear the HashMaps, so that off-line players are gone
			prefixes.clear();
			displayNames.clear();
			suffixes.clear();

			// Now get all online players
			Player[] players = Bukkit.getServer().getOnlinePlayers();

			// Loop them and put their info in the HashMaps
			for (Player loopPlayer : players) {
				String mcname = loopPlayer.getName();

				prefixes.put(mcname, chat.getPlayerPrefix(loopPlayer));
				displayNames.put(mcname, loopPlayer.getDisplayName());
				suffixes.put(mcname, chat.getPlayerSuffix(loopPlayer));
			}

			// Now to actually send the data to the clients, oh boy
			for (Player receiver : meta.registeredPlayers) {
				for (Player loopPlayer : players) {
					String mcname = loopPlayer.getName();

					// Using \n as delimiter
					String data = String.format("%s\n%s\n%s\n%s",
							mcname,
							prefixes.get(mcname),
							displayNames.get(mcname),
							suffixes.get(mcname)
							);

					receiver.sendPluginMessage(instance, channelData, data.getBytes());
				}
			}
		}
	}

	private boolean setupChat()
	{
		RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
		if (chatProvider != null) {
			chat = chatProvider.getProvider();
		}

		return (chat != null);
	}

	private void reloadPlugin()
	{
		this.reloadConfig();
		delayTicks = Long.valueOf(this.getConfig().getInt("delayTicks"));
		
		scheduleTask();		
	}
	
	private void scheduleTask()
	{
		if (task != null) {
			try {
				task.cancel();
				log.info(String.format("[%s] Canceled task %d", getDescription().getName(), task.getTaskId()));
			} catch (IllegalStateException e) {
				log.warning(String.format("[%s] Could not cancel task. It might not be scheduled.",
						getDescription().getName()));
			}
		}

		try {
			task = new ScheduledTask().runTaskTimer(this, 0L, delayTicks);
			log.info(String.format("[%s] Scheduled task with interval %d and TaskId %d", 
					getDescription().getName(),
					delayTicks,
					task.getTaskId()
					));
		} catch (IllegalStateException e) {
			log.severe(String.format("[%s] Could not schedule task, as it was already scheduled!", getDescription().getName()));
		}
	}
}
