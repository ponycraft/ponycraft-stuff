package net.ponycraft.lobe.nametags.bukkit;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

class MetaChannelListener implements PluginMessageListener {
	private static final Logger log = Logger.getLogger("Minecraft");
	private PlayerNametags instance;
	
	public String channel;
	
	public List<Player> registeredPlayers = new ArrayList<>();
	
	public MetaChannelListener(PlayerNametags aInstance, String aChannel) {
		channel = aChannel;
		instance = aInstance;
		Bukkit.getMessenger().registerIncomingPluginChannel(instance, channel, this);
		Bukkit.getMessenger().registerOutgoingPluginChannel(instance, channel);
	}
	
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		if (message.toString().equalsIgnoreCase("REGISTER")) {
			registerPlayer(player);
			player.sendPluginMessage(instance, channel, "OK REGISTER".getBytes());
			return;
		}
		if (message.toString().equalsIgnoreCase("UNREGISTER")) {
			unregisterPlayer(player);
			player.sendPluginMessage(instance, channel, "OK UNREGISTER".getBytes());
			return;
		}
		if (message.toString().equalsIgnoreCase("REGISTERED")) {
			player.sendPluginMessage(instance, channel, String.valueOf(
					registeredPlayers.contains(player.getName())).toUpperCase().getBytes());
			return;
		}
	}
	
	public void registerPlayer(Player player) {
		if (!registeredPlayers.contains(player)) {
			registeredPlayers.add(player);
			log.info(String.format("[%s] Registered client %s", instance.getDescription().getName(), player.getName()));
		}
	}
	
	public void unregisterPlayer(Player player) {
		if (registeredPlayers.contains(player)) {
			registeredPlayers.remove(player);
			log.info(String.format("[%s] Unregistered client %s", instance.getDescription().getName(), player.getName()));
		}
	}
}
